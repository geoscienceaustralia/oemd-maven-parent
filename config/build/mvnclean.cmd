@echo off
:: @author Laurie Chan
:: execute maven clean on all subdirectories (of the elated workspace) containing a pom file
::
echo %0 %1
setLocal
set arg=%1
if "%arg%"=="-?" goto :usage
if "%arg%"=="-h" goto :usage
if "%arg%"=="-H" goto :usage
set PATH=%PATH%;%JAVA_HOME%\bin;

call %arg% %2 %3
::@echo on

for /D %%f in (*) do call :mvnclean "%%f"
endLocal
goto :EOF

:mvnclean
set fp=%1
cd %fp%
if not exist pom.xml goto :nopom
echo cleaning %fp%
call mvn clean -o
:nopom
cd ..
goto :EOF


:usage
echo mvnclean
echo .
goto :EOF
