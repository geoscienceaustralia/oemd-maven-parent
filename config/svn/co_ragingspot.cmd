@echo off 
echo %0 %1 %2 %3
set ws=%~p0..\..\..
echo ws=%ws%
cd "%ws%"
echo checking out raging spot subprojects to %ws%
pause
:: svn co http://www.ga.gov.au:9080/svn/oemd/OEMD_IS/common/framework/oemd-maven-parent/trunk oemd-maven-parent
svn co http://www.ga.gov.au:9080/svn/oemd/OEMD_IS/common/framework/oemd-common-service/trunk oemd-common-service
svn co http://www.ga.gov.au:9080/svn/oemd/OEMD_IS/common/framework/oemd-unit-test/trunk oemd-unit-test
svn co http://www.ga.gov.au:9080/svn/oemd/OEMD_IS/common/framework/oemd-common-web/trunk oemd-common-web
svn co http://www.ga.gov.au:9080/svn/oemd/OEMD_IS/common/framework/oemd-web-template/trunk oemd-web-template
svn co http://www.ga.gov.au:9080/svn/oemd/OEMD_IS/geochronology/sample-analyses/geochron-sa-service/trunk geochron-sa-service
svn co http://www.ga.gov.au:9080/svn/oemd/OEMD_IS/geochronology/sample-analyses/geochron-sa-web/trunk geochron-sa-web
svn co http://www.ga.gov.au:9080/svn/oemd/OEMD_IS/geochemistry/sample-analyses/geochem-sa-service/trunk geochem-sa-service
svn co http://www.ga.gov.au:9080/svn/oemd/OEMD_IS/geochemistry/sample-analyses/geochem-sa-web/trunk geochem-sa-web
svn co http://www.ga.gov.au:9080/svn/oemd/OEMD_IS/common/framework/oemd-web-portal/trunk oemd-web-portal
pause